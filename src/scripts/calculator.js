;
(function ($, window, document, undefined) {

	/**
	 * Global Vars
	 */
	var daysArray = ['день', 'дня', 'дней'];

	var calculaRate = 15,
		calculaFactor = 12,
		calculaAmountMin = 1000,
		calculaAmountMax = 15000,
		calculaTermMin = 1,
		calculaTermMax = 12,
		calculaRange = false,
		calculaAmountHidden,
		calculaTermHidden;

	/**
	 * Default options for the plugin.
	 * @type {Object}
	 */
	var defaults = {
		// types
		calculaMainType: 'credit',
		calculaTermType: 'month',

		// selectors
		calculaSectionSelector: '.calculator__section',
		calculaAmountSelector: '.calculator__section-amount',
		calculaTermSelector: '.calculator__section-term',
		calculaRangeSelector: '.calculator__range',

		// slide
		calculaSlideSelector: '.calculator__range-col-change',
		calculaSlideRangeDescSelector: '.calculator__range-val',
		calculaSlideRangeSelectSelector: '.calculator__range-selected',
		calculaSlideHandlerSelector: '.calculator__range-switch',

		// results
		calculaResultGetSelector: '.calculator__result-get',
		calculaResultTotalSelector: '.calculator__result-total',
		calculaResultRateSelector: '.calculator__result-rate',
		calculaResultProfitSelector: '.calculator__result-profit',
		calculaResultProfitMonthSelector: '.calculator__result-profit-month',
		calculaResultPercentSelector: '.calculator__result-percent',
		calculaResultPercentMonthSelector: '.calculator__result-percent-month',
		calculaResultHoldSelector: '.calculator__result-hold',
		calculaResultHoldMonthSelector: '.calculator__result-hold-month',


		// values
		calculaNotPercent: 1,
		calculaRefRate: 16,
		calculaRate: calculaRate,
		calculaFactor: calculaFactor,
		calculaAmountMin: calculaAmountMin,
		calculaAmountMax: calculaAmountMax,
		calculaAmountStep: calculaAmountMin,
		calculaAmountVal: calculaAmountMin,
		calculaTermMin: calculaTermMin,
		calculaTermMax: calculaTermMax,
		calculaTermStep: calculaTermMin,
		calculaTermVal: calculaTermMin,
		calculaRange: calculaRange,

	};

	/**
	 * Plugin constructor
	 * @param {Object} element
	 * @param {Object} options
	 */
	function Plugin(element, options) {
		this.$el = $(element);
		this._name = 'calcula';
		this._defaults = defaults;
		this.settings = $.extend({}, defaults, options);
		this.initRender();
		this.init();
		this.initSlider();
		this.rateArray;
	}


	// Avoid Plugin.prototype conflicts

	Plugin.prototype = {

		/**
		 * Shows the results.
		 * @return {void}
		 */
		init: function () {
			this.render();
		},

		/**
		 * Show the first result in the DOM.
		 * @return {void}
		 */
		initRender: function () {
			var _this = this;
			
			this.settings = $.extend({}, this._defaults, {
				calculaAmountVal: this.ejectData(this.settings.calculaAmountSelector, 'value'),

				calculaTermVal: this.ejectData(this.settings.calculaTermSelector, 'value'),

				calculaAmountMin: this.ejectData(this.settings.calculaAmountSelector, 'min'),

				calculaAmountMax: this.ejectData(this.settings.calculaAmountSelector, 'max'),

				calculaAmountStep: this.ejectData(this.settings.calculaAmountSelector, 'step'),

				calculaTermMin: this.ejectData(this.settings.calculaTermSelector, 'min'),

				calculaTermMax: this.ejectData(this.settings.calculaTermSelector, 'max'),

				calculaTermStep: this.ejectData(this.settings.calculaTermSelector, 'step'),

				calculaRate: this.ejectData(this.$el, 'rate') || this.settings.calculaRate,

				calculaFactor: this.ejectData(this.$el, 'factor') || this.settings.calculaFactor,

				calculaMainType: this.$el.data('type') || this.settings.calculaMainType,

				calculaTermType: this.$el.data('term') || this.settings.calculaTermType,
				
				calculaNotPercent: this.$el.data('startRate') || this.settings.calculaNotPercent,
				
				calculaRefRate: this.$el.data('refRate') || this.settings.calculaRefRate,
				
				calculaRange: this.$el.data('range') || this.settings.calculaRange,
			})
			
			if(Object.prototype.toString.call(this.settings.calculaRate) === '[object Array]'){
				this.rateArray = this.settings.calculaRate
			}


			var $amountRangeDesc = this.$el.find([this.settings.calculaAmountSelector,
					this.settings.calculaSlideRangeDescSelector].join(' ')),
				$termRangeDesc = this.$el.find([this.settings.calculaTermSelector,
					this.settings.calculaSlideRangeDescSelector].join(' '));

			calculaAmountHidden = this.$el.find([this.settings.calculaAmountSelector,
					'input[type="hidden"]'].join(' ')
			),
				calculaTermHidden = this.$el.find([this.settings.calculaTermSelector,
						'input[type="hidden"]'].join(' ')
				);

			calculaAmountHidden.val(this.settings.calculaAmountVal)
			calculaTermHidden.val(this.settings.calculaTermVal)
			
			
			// Amount Descriptio Show
			$amountRangeDesc.each(function (index) {
				var $this = $(this);
				$this.html(
					index === $amountRangeDesc.length - 1 ?
					_this.settings.calculaAmountMax + '&nbsp;&#8381;' :
					_this.settings.calculaAmountMin + '&nbsp;&#8381;'
				)
			})
			// Term Description Show
			$termRangeDesc.each(function (index) {
				var $this = $(this);

				function labelFn(value) {
					return _this.settings.calculaTermType == "days" ?
					'&nbsp;' + _this.declension(value, daysArray) :
						'&nbsp;мес.'
				}

				if (index === 1) {
					$this.html(
						Math.floor(_this.settings.calculaTermMax / 2) + labelFn(Math.round(_this.settings.calculaTermMax / 2))
					)
				} else {
					$this.html(
						index === $termRangeDesc.length - 1 ?
						_this.settings.calculaTermMax + labelFn(_this.settings.calculaTermMax) :
						_this.settings.calculaTermMin + labelFn(_this.settings.calculaTermMin)
					)
				}
			})
		},

		/**
		 * Init sliders.
		 * @return {void}
		 */
		initSlider: function () {
			var _this = this;
			function checkSlider(selector, checkClass, result) {
				checkClass = checkClass.indexOf('.') == 0 ? checkClass.substring(1) : checkClass
				if (selector.hasClass(checkClass)) {
					return result[0];
				} else {
					return result[1];
				}
			}


			this.$el.find(this.settings.calculaSectionSelector).each(function () {
				var $this = $(this);


				var slVal = checkSlider(
						$this,
						_this.settings.calculaAmountSelector,
						[_this.settings.calculaAmountVal, _this.settings.calculaTermVal]
					),
					slMin = checkSlider(
						$this,
						_this.settings.calculaAmountSelector,
						[_this.settings.calculaAmountMin, _this.settings.calculaTermMin]
					),
					slMax = checkSlider(
						$this,
						_this.settings.calculaAmountSelector,
						[_this.settings.calculaAmountMax, _this.settings.calculaTermMax]
					),
					slStep = checkSlider(
						$this,
						_this.settings.calculaAmountSelector,
						[_this.settings.calculaAmountStep, _this.settings.calculaTermStep]
					);

				$this.find(_this.settings.calculaSlideSelector).slider({
					min: slMin,
					max: slMax,
					range: _this.settings.calculaRange,
					value: slVal,
					step: slStep,
					handle: _this.settings.calculaSlideHandlerSelector,

					create: function (event, ui) {
						_this.$el.addClass('in')
					},
					slide: function (event, ui) {
						
						if (_this.hasClassHandler($this, _this.settings.calculaAmountSelector)){
							_this.settings.calculaAmountVal = ui.value
						} else {
							_this.settings.calculaTermVal = ui.value
						}
						_this.init();
					},
				});
			})
		},
		
		
		hasClassHandler: function(selector, checked){
			checked = checked.indexOf('.') == 0 ? checked.substring(1) : checked;
			return selector.hasClass(checked);
		},


		/**
		 * Eject data function
		 * @params {String} selector
		 * @params {String} dataName
		 */
		ejectData: function (selector, dataName) {
			var sel = this.$el.find(selector).data(dataName);
			if (selector === this.$el) {
				sel = this.$el.data(dataName)
				if(Object.prototype.toString.call(sel) === '[object Array]'){
					return sel
				}
				return parseFloat(sel)
			} else {
				return parseFloat(sel)
			}

		},

		/**
		 * Add values function
		 * @params {String} selector
		 * @params {String} value
		 * @params {String} label
		 */
		addValues: function(selector, value, label){
			if(label == 'value'){
				label = '&nbsp;&#8381;'
			}
			else if(label == 'term'){
				label = this.settings.calculaTermType == 'days' ?
				'&nbsp;' + this.declension(value, daysArray) :
				'&nbsp;мес.'
			}
			else if(label == 'rate'){
				label = '&#37;'
			} else{
				label = ''
			}
			return this.$el.find(selector).html(
				value + label
			)
		},

		/**
		 * Rendered and show result in the DOM.
		 * @return {void}
		 */
		render: function () {
			var _this = this;

			var amountVal = this.settings.calculaAmountVal,
				termVal = this.settings.calculaTermVal,
				amountSelected = [this.settings.calculaAmountSelector,
					this.settings.calculaSlideRangeSelectSelector].join(' '),
				termSelected = [this.settings.calculaTermSelector,
					this.settings.calculaSlideRangeSelectSelector].join(' ');

			// Check rateArray
			if(this.rateArray){
				$.each(this.rateArray, function(index, item){
					var itemParent = item;
					if(Object.prototype.toString.call(item[1]) === '[object Array]'){
						if(item[1].length > 1){
							$.each(item[1], function(index, item){
								if(_this.settings.calculaAmountVal >= itemParent[0] && _this.settings.calculaTermVal >= item[0]){
									_this.settings.calculaRate = item[1]
								}
							})
						} else{
							if(_this.settings.calculaAmountVal >= item[0] && _this.settings.calculaTermVal >= item[1][0]){
								_this.settings.calculaRate = item[1][1]
							}
						}
					} else{
						if(_this.settings.calculaAmountVal >= item[0]){
							_this.settings.calculaRate = item[1]
						}
					}
				})
				
			}
			
			// display Values
			this.addValues(this.settings.calculaResultGetSelector, this.settings.calculaAmountVal, 'value')
			
			if(this.settings.calculaMainType == 'credit'){
				this.addValues(this.settings.calculaResultTotalSelector, parseFloat(this.calcPayment().toFixed(2)), 'value')
			} else{
				this.addValues(this.settings.calculaResultTotalSelector, this.calcTotal(), 'value')
			}

			if(this.settings.calculaMainType == 'deposit'){
				
				this.addValues(this.settings.calculaResultRateSelector, this.settings.calculaRate, 'rate')
				
				this.addValues(this.settings.calculaResultProfitSelector, this.calcProfitTotal())
				
				this.addValues(this.settings.calculaResultProfitMonthSelector, this.calcProfit().toFixed(2))
				
				this.addValues(this.settings.calculaResultPercentSelector, this.calcPercentTotal())
				
				this.addValues(this.settings.calculaResultPercentMonthSelector, this.calcPercent().toFixed(2))
				
				this.addValues(this.settings.calculaResultHoldSelector, this.calcHoldTotal())
				
				this.addValues(this.settings.calculaResultHoldMonthSelector, this.calcHold().toFixed(2))
			}

//			console.log('calculaNotPercent', this.settings.calculaNotPercent)
//			console.log('calculaRefRate', this.settings.calculaRefRate)
//			console.log('calculaRate', this.settings.calculaRate)
//			console.log('calcRate', this.calcRate())
//			console.log('calcPayment', this.calcPayment())
//			console.log('calcTotal', this.calcTotal())
//			console.log('calcPercent', this.calcPercent())
//			console.log('---------------------------------------------')


			this.addValues(amountSelected, amountVal, 'value')

			this.addValues(termSelected, termVal, 'term')
		},


		/**
		 * Run the init method again with the provided options.
		 * @param  {Object} args
		 */
		update: function (args) {
			this.settings = $.extend({}, this._defaults, args);
			this.init();
		},

		/**
		 * Calculate Rate
		 * @return {void}
		 */
		calcRate: function () {
			return (this.settings.calculaRate / 100) / this.settings.calculaFactor
		},

		/**
		 * Calculate Payment
		 * @return {void}
		 */
		calcPayment: function () {
			if(
			   this.settings.calculaMainType == 'micro'){
				return this.settings.calculaAmountVal * this.calcRate()
			} else{
				return this.settings.calculaAmountVal * this.calcRate() / (1 - 1 / Math.pow(1 + this.calcRate(), this.settings.calculaTermVal));
			}
		},

		/**
		 * Calculate Profit for the entire period
		 * @return {void}
		 */
		calcProfit: function() {
			return this.calcProfitTotal() / this.settings.calculaTermVal
		},

		/**
		 * Calculate Profit for all period
		 * @return {void}
		 */
		calcProfitTotal: function() {
			return Math.round(this.calcPercentTotal() - this.calcHoldTotal())
		},

		/**
		 * Calculate Percent for the entire period
		 * @return {void}
		 */
		calcPercent: function () {
			if(this.settings.calculaMainType == 'deposit'){
				return this.calcPercentTotal() / this.settings.calculaTermVal
			}
		},

		/**
		 * Calculate Percent for the all period
		 * @return {void}
		 */
		calcPercentTotal: function () {
			if(this.settings.calculaMainType == 'deposit'){
				return Math.round((
					this.settings.calculaAmountVal * 
					this.settings.calculaRate * 
					this.settings.calculaTermVal) / ( 100 * this.settings.calculaFactor))
			} else{
				return this.calcPayment() * this.settings.calculaTermVal - this.settings.calculaAmountVal
			}
		},

		/**
		 * Calculate Hold for the entire period
		 * @return {void}
		 */
		calcHold: function () {
			return this.calcHoldTotal() / this.settings.calculaTermVal
		},

		/**
		 * Calculate Hold for the all period
		 * @return {void}
		 */
		calcHoldTotal: function () {
			if(this.settings.calculaRate <= this.settings.calculaRefRate){
				return 0
			} else{
				return Math.round((this.calcPercentTotal() - ((
					this.settings.calculaAmountVal * 
					this.settings.calculaRefRate * 
					this.settings.calculaTermVal) / ( 100 * this.settings.calculaFactor))) * 0.35)
			}
			
		},

		/**
		 * Calculate Total
		 * @return {void}
		 */
		calcTotal: function () {
			if(this.settings.calculaMainType == 'micro'){
				if(this.settings.calculaNotPercent > 1){
					if(this.settings.calculaTermVal < this.settings.calculaNotPercent){
						return this.settings.calculaAmountVal
					} else{
						return Math.ceil(this.settings.calculaAmountVal + this.calcPayment() * (this.settings.calculaTermVal - this.settings.calculaNotPercent))
					}
				} else{
					return Math.ceil(this.settings.calculaAmountVal + this.calcPayment() * this.settings.calculaTermVal)
				}
			}
			else if(this.settings.calculaMainType == 'deposit'){
				return Math.round(this.settings.calculaAmountVal + this.calcPercentTotal() - this.calcHoldTotal())
			}
			else{
				return Math.ceil(this.calcPayment() * this.settings.calculaTermVal)
			}
		},

		/**
		 * Declension ending words
		 * @param  {Number} num
		 * @param  {Array} expression
		 */
		declension: function (num, expressions) {
			var result,
				count = num % 100;

			if (count >= 5 && count <= 20) {
				result = expressions['2'];
			} else {
				count = count % 10;
				if (count == 1) {
					result = expressions['0'];
				} else if (count >= 2 && count <= 4) {
					result = expressions['1'];
				} else {
					result = expressions['2'];
				}
			}
			return result;
		},

	};


	$.fn.calcula = function (options, args) {


		return this.each(function (index, element) {

//			new Plugin(element, options);
			
			var instance = $(this).data( 'plugin_calcula');
			if (! instance) {
			$(this).data('plugin_calcula', new Plugin(this, options));
			}
			else if (options === 'update') {
			return instance.update(args);
			}
		});
	};

})(jQuery, window, document);

