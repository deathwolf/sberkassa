
	var myMap,
		myGeocoder,
		mapAddress,
		myCollection,
		geoObjects,
		geoRegion,
		geoCity;


	var $contactsRender = $('.contacts-info-render');
	var navCity = $contactsRender.find('.nav');

//	ymaps.load(function(){
//		$('#map-contacts').removeClass('loading')
//		init()
//	});
	ymaps.ready(init);

	function init(){
		var geolocation = ymaps.geolocation;
		
		myMap = new ymaps.Map('map-contacts', {
			center: [56.258448, 90.488341],
			zoom: 10,
			controls: [],
		}, {
			searchControlProvider: 'yandex#search'
		});
		myCollection = new ymaps.GeoObjectCollection();
		
		myMap.behaviors.disable('scrollZoom')
		
		myMap.behaviors.disable('drag')

		myMap.controls.add('zoomControl');
		
		
			
		$.getJSON('scripts/data.json', function (json) {
			var length = json.length;
			
			filterMarks(json, true)

			$.each(sameValues(json, 'city'), function(index, item){
				$contactsRender.find('.selectpicker').append('<option value="'+item+'">'+item+'</option>')
				$('.selectpicker').selectpicker('refresh')
			})
			
			geolocation.get({
				provider: 'yandex',
				mapStateAutoApply: true
			}).then(function (result) {
					geoRegion = result.geoObjects.get(0).properties.get('metaDataProperty.GeocoderMetaData.AddressDetails.Country.AdministrativeArea.AdministrativeAreaName');
				
					geoCity = (result.geoObjects.get(0).properties.get('metaDataProperty.GeocoderMetaData.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality.LocalityName'))
					
					$contactsRender.find('.selectpicker option').each(function(){
						if(geoCity == $(this).text()){
							$(this).parent().val(geoCity).change()
							$('.selectpicker').selectpicker('refresh')
						} else{
							$(this).parent().eq(0).change()
							$('.selectpicker').selectpicker('refresh')
						}
					})
			});




			$contactsRender.find('.selectpicker').on('change', function(){
				var $this = $(this),
					value = $this.val();
					
				myCollection.removeAll();
				
				$.each(json, function(index, item){
					if(value == item.address.city){
						filterMarks(item)
					}
				})
				
				geoObjects = ymaps.geoQuery(myCollection)
					.addToMap(myMap)
					.applyBoundsToMap(myMap, {
						checkZoomRange: true
					});
				geoObjects.then(function() {
					var p = geoObjects.get(0);
					p.balloon.open();
				});
				
			})
			
			


		}).complete(function(){
			setTimeout(function(){
				$('#map-contacts').removeClass('loading')
			}, 1000)
		})

		
	};
				
	function filterMarks(data, geoInit){
		
		geoInit = geoInit || false
		
		if(geoInit){
			$.each(data, function(index, item){
				placemark = new ymaps.Placemark(item.coordinates,{
					balloonContent: '<div class="balloon-content"><div class="container-inner"><img src="'+item.properties.balloonContentImages+'"/><div class="balloon-content__desc"><div class="balloon-content__desc-head">'+item.properties.balloonContentHead+'</div><div class="balloon-content__desc-body">'+item.properties.balloonContent+'</div></div></div>',
				},{
					"iconLayout": "default#image",
					"iconImageHref": "images/mark-mob.png",
					"iconImageSize": [52, 66],
					"balloonPanelMaxMapArea": Infinity,
					"balloonAutoPan": false,
					"balloonCloseButton": false
				})
				myCollection.add(placemark)
			})
			geoObjects = ymaps.geoQuery(myCollection)
				.addToMap(myMap)
				.applyBoundsToMap(myMap, {
					checkZoomRange: true
				});
		} else{
			placemark = new ymaps.Placemark(data.coordinates,{
				balloonContent: '<div class="balloon-content"><div class="container-inner"><img src="'+data.properties.balloonContentImages+'"/><div class="balloon-content__desc"><div class="balloon-content__desc-head">'+data.properties.balloonContentHead+'</div><div class="balloon-content__desc-body">'+data.properties.balloonContent+'</div></div></div>',
			},{
				"iconLayout": "default#image",
				"iconImageHref": "images/mark-mob.png",
				"iconImageSize": [52, 66],
				"balloonPanelMaxMapArea": Infinity,
				"balloonAutoPan": false,
				"balloonCloseButton": false
			})
			myCollection.add(placemark)
		}

		
	}

	function sameValues(array, type){
		type = type || ''
		
		var ArrayWithUniqueValues = [];

		var objectCounter = {};

		for (i = 0; i < array.length; i++) {
			if(type == 'region'){
				var currentMemboerOfArrayKey = JSON.stringify(array[i].address.region);
				var currentMemboerOfArrayValue = array[i].address.region;
			}
			else if(type == 'city'){
				var currentMemboerOfArrayKey = JSON.stringify(array[i].address.city);
				var currentMemboerOfArrayValue = array[i].address.city;
			}
			

			if (objectCounter[currentMemboerOfArrayKey] === undefined){
				ArrayWithUniqueValues.push(currentMemboerOfArrayValue);
				 objectCounter[currentMemboerOfArrayKey] = 1;
			}else{
				objectCounter[currentMemboerOfArrayKey]++;
			}
		}
		return ArrayWithUniqueValues;
	}