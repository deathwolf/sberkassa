;(function($, window, document, undefined) {
	"use strict";
	
	var formSteps = $(".steps-app").show();

	// Validation date birth
	$.validator.addMethod("dbirthDate", function(value, element, params) {

		var minAge = params[0] || 18,
			maxAge = params[1] || 55;
			
		var currDate = new Date(),
			changeDate = value.split("/"),
			userMinDate = new Date((parseInt(changeDate[2]) + minAge), (parseInt(changeDate[1])-1), changeDate[0]),
			userMaxDate = new Date((parseInt(changeDate[2]) + maxAge), (parseInt(changeDate[1])-1), (parseInt(changeDate[0])+1));
			
		
		var diffMin = currDate - userMinDate,
			diffMax = userMaxDate - currDate;

		
		if(diffMin > -1 && diffMax > 1){
			return this.optional(element) || /^\d\d?\/\d\d?\/\d\d\d\d$/.test(value);
		}
}, 'Ваш возраст должен быть от {0} до {1} лет');

	// Validation passport Code
	$.validator.addMethod("passCode", function (value, element) {
		return this.optional(element) || /^\d{3}\ \d{3}?$/.test(value);
	}, 'Поле должно иметь вид <br><u>xxx</u>&nbsp;<u>xxx</u>');

	// Validation passport ID
	$.validator.addMethod('passId', function(value, element){
		return this.optional(element) || /^\d{2}\ \d{2} \d{6}?$/.test(value);
	}, 'Поле должно иметь вид <br><u>xx</u>&nbsp;<u>xx</u>&nbsp;<u>xxx</u>')

	// Validation Phone
	$.validator.addMethod("mobileFormat", function (value, element) {
		return this.optional(element) || /^\(\d{3}\) \d{3}\-\d{2}-\d{2}?$/.test(value);
	}, 'Введите корректный номер телефона.');



	formSteps.steps({
		headerTag: "h3",
		bodyTag: "section",
		transitionEffect: "slideLeft",
		cssClass: 'steps-app',
		saveState: true,
		labels: {
			cancel: "Отмена",
			current: "",
			pagination: "Pagination",
			finish: "Отправить заявку",
			next: "Продолжить",
			previous: "Вернуться на шаг назад",
			loading: "Загрузка ..."
		},
		titleTemplate: '<span class="steps-text">#title#</span>',
		onInit: function(event, currentIndex){
//			$('.online-app .nav-pills a').on('click', function(){
//				var $this = $(this),
//					idTarget = $this.data('target').split(', '),
//					$infoContainer = $this.parents('.online-app').eq(0).find('.services-info'),
//					$infoTab = $infoContainer.find('.tab-content '+idTarget[1]+''),
//					$infoTabAll = $infoContainer.find('.tab-content .tab-pane');
//			})
		},
		onStepChanging: function (event, currentIndex, newIndex)
		{
			// Allways allow previous action even if the current form is not valid!
			if (currentIndex > newIndex)
			{
				return true;
			}
			// Needed in some cases if the user went back (clean up)
			if (currentIndex < newIndex)
			{
				// To remove error styles
				formSteps.find(".body:eq(" + newIndex + ") label.error").remove();
				formSteps.find(".body:eq(" + newIndex + ") .error").removeClass("error");
			}
			
			formSteps.validate().settings.ignore = ":disabled,:hidden";
			return formSteps.valid();
		},
		onStepChanged: function (event, currentIndex, priorIndex)
		{
			
		},
		onFinishing: function (event, currentIndex)
		{
			formSteps.validate().settings.ignore = ":disabled";
			return formSteps.valid();
		},
		onFinished: function (event, currentIndex) // Отправка данных
		{
			formSteps.find('.content').html('<div class="h2">Ваша заявка успешно отправлена!</div>')
			formSteps.find('.actions').remove()
		}
	}).validate({
		lang: 'ru',
		onkeyup: function(element) {$(element).valid()},
		rules: {
			getDate:{
				dateNL: true
			},
			
		},
		errorPlacement: function errorPlacement(error, element) { element.before(error);},
	});
	
	$.validator.addClassRules({
		"date": {
			dateNL: true
		},
	});
	$('#eq-address').on('change', function () {
		var disabledBlock = $(this).data('disable');
		$(this).is(':checked') ? $('[name*="'+disabledBlock+'"]').attr("disabled", true).removeClass('error') : $('[name*="'+disabledBlock+'"]').attr("disabled", false).removeClass('error')
	 });
	
	$('.form-control.date, .date > .form-control').datepicker({
		format: "dd/mm/yyyy",
		language: "ru-RU",
		autoclose: true,
		container: '.main-container-wrap',
		clearBtn: true,
		startView: 2,
	}).on('changeDate', function(ev) {
		$(this).valid()
	 });
	

	if($('body').hasClass('mobile-body')){
		$(":input").mask();
		$("[name='mobNum']").mask('(999) 999-99-99')
		$("[name='passGetId']").mask('99 99 999999')
		$("[name='passGetCode']").mask('999 999')
	} else{
		$(":input").inputmask();
		$("[name='mobNum']").inputmask({
			mask: '(999) 999-99-99',
			placeholder: '_',
			clearMaskOnLostFocus: false,
			//autoUnmask: true,
			oncomplete: function(){
			}
		})
		$("[name='passGetId']").inputmask({
			mask: '99 99 999999',
			placeholder: '_',
			clearMaskOnLostFocus: false,
			//autoUnmask: true,
			oncomplete: function(){
			}
		})
		$("[name='passGetCode']").inputmask({
			mask: '999 999',
			placeholder: '_',
			clearMaskOnLostFocus: false,
			//autoUnmask: true,
			oncomplete: function(){
			}
		})
	}

})(jQuery, window, document);