$(function(){
	$('.loan-payment__link').on('click', function(e){
		$(this).hasClass('disabled') && e.preventDefault();
	})


	// Validation
	var $loanForm = $('.loan-payment form');

	$loanForm
	.validate({
		lang: 'ru',
		onkeyup: function(element) {
			$(element).valid()
			if($loanForm.find(':required').length == $loanForm.find('input.valid').length){
				$loanForm.find('.loan-payment__link').removeClass('disabled')
			} else{
				$loanForm.find('.loan-payment__link').addClass('disabled')
			}
		},
		errorPlacement: function errorPlacement(error, element) { element.before(error);},
	});

	// Validation Phone
	$.validator.addMethod("mobileFormat", function (value, element) {
		return this.optional(element) || /^\(\d{3}\) \d{3}\-\d{2}-\d{2}?$/.test(value);
	}, 'Введите корректный номер телефона.');
	if($('body').hasClass('mobile-body')){
		$(":input").mask();
		$("[name='paymentMobNum']").mask('(999) 999-99-99')
	} else{
		$(":input").inputmask();
		$("[name='paymentMobNum']").inputmask({
			mask: '(999) 999-99-99',
			placeholder: '_',
			clearMaskOnLostFocus: false,
			//autoUnmask: true,
			oncomplete: function(){
			}
		})
	}
})
