var rupture = require('rupture');
var autoprefixer = require('gulp-autoprefixer');

module.exports = function (options) {

	gulp.task('styles', function () {

		var stylusOptions = {
			errors: true,
			use: rupture(),
			sourcemap: true
		};
		return gulp.src(options.files.stylus)
			.pipe($.plumber({errorHandler: options.errorHandler("styles")}))
			.pipe($.stylus(stylusOptions))
			.pipe(autoprefixer({
				browsers: ['> 1%'],
				cascade: false
			}))
			.pipe($.minifyCss())
			.pipe($.csscomb())
			.pipe(gulp.dest(options.paths.dist + '/styles'))
			.pipe(browserSync.stream());
	});

};
