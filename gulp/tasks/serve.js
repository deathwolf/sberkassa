var browserSync = require('browser-sync');

module.exports = function (options) {

	gulp.task('serve', ['build', 'watch'], function (done) {
		browserSync({
			open: false,
			port: 9000,
			server: {
				baseDir: [options.paths.dist],
				middleware: function (req, res, next) {
					res.setHeader('Access-Control-Allow-Origin', '*');
					next();
				}
			}
		});
	});
};
