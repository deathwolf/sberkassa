var gulp = global.gulp = require('gulp');
var $ = global.$ = require('gulp-load-plugins')();
global.browserSync = require('browser-sync');

var gutil = require('gulp-util');
var wrench = require('wrench');

var options = {
	files: {
		js: 'src/scripts/**',
		html: 'src/**/*.html',
		jade: 'src/pages/**/*.jade',
		pages: 'src/pages/**.jade',
		stylus: 'src/styles/*.styl',
		stylusAll: 'src/**/**.styl',
		style: 'styles/**/*.css',
		images: 'src/images/**',
		sprite: 'src/images/sprite/*.*',
		fonts: 'src/fonts/**'
	},
	paths: {
		src: 'src',
		test: 'test',
		dist: 'dist',
		tmp: '.tmp',
		e2e: 'e2e'
	},
	errorHandler: function (title) {
		return function (err) {
			gutil.log(gutil.colors.red('[' + title + ']'), err.toString());
			this.emit('end');
		};
	}
};

wrench.readdirSyncRecursive('./gulp/tasks').filter(function (file) {
	return (/\.(js|coffee)$/i).test(file);
}).map(function (file) {
	require('./gulp/tasks/' + file)(options);
});

gulp.task('default', ['clean'], function () {
	gulp.start('build');
});
